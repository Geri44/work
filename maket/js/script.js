<script>
$(function () {
  $('#carousel').carousel({
    interval: 1000,
    keyboard: false,
    pause: 'hover',
    ride: 'carousel',
    wrap: false
  })
  });
});
</script>